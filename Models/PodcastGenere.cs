﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiapple.Models
{
    public class PodcastGenere
    {
        public int Id { get; set; }
        public int PodCastId { get; set; }
        public string GenreId { get; set; }
        public virtual Genere Genre { get; set; }
    }
}
