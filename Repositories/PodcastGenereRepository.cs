﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.Data;
using apiapple.Models;

namespace apiapple.Repositories
{
    public class PodcastGenereRepository : IPodcastGenereRepository
    {
        private DataContext _dataContext;

        public PodcastGenereRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Create(PodcastGenere podcastGenere)
        {
            _dataContext.PodcastGeneres.Add(podcastGenere);
        }

        public void CreateRange(List<PodcastGenere> podcastGeneres)
        {
            _dataContext.PodcastGeneres.AddRange(podcastGeneres);
        }
    }
}
