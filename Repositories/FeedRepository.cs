﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.Data;
using apiapple.DTO;
using apiapple.Models;
using Microsoft.EntityFrameworkCore;

namespace apiapple.Repositories
{
    public class FeedRepository :IFeedRepository
    {
        private readonly DataContext _dataContext;

        public FeedRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<FeedDto> GetFeed()
        {
            var podCast = await _dataContext.Feeds.Include(x => x.Results).ThenInclude(x => x.PodcastGeneres).ThenInclude(x => x.Genre).Include(x => x.Author).Include(x => x.Link).FirstOrDefaultAsync();

            if (podCast == null)
            {
                return new FeedDto();
            }
            var feedDto = new FeedDto
            {
                Title = podCast.Title,
                Id = podCast.Id,
                Author = podCast.Author,
                Link = podCast.Link,
                Copyright = podCast.Copyright,
                Country = podCast.Country,
                Icon = podCast.Icon,
                Updated = podCast.Updated
            };

            var podCastDto = podCast.Results.OrderBy(x => x.Top).Select(x =>
                new PodcastDto
                {
                    ArtistName = x.ArtistName,
                    Id = x.Id,
                    ReleaseDate = x.ReleaseDate,
                    Name = x.Name,
                    Kind = x.Kind,
                    Copyright = x.Copyright,
                    ArtworkUrl100 = x.ArtworkUrl100,
                    Url = x.Url,
                    ArtistId = x.ArtistId,
                    ContentAdvisoryRating = x.ContentAdvisoryRating,
                    Genres = x.PodcastGeneres.Select(y => y.Genre).ToList(),
                    Top = x.Top,
                }).ToList();

            feedDto.Results = podCastDto;
            return feedDto;
        }

        public void Create(Feed a)
        {
            _dataContext.Feeds.Add(a);
        }
    }
}
