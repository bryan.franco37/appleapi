﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.DTO;
using apiapple.Models;
using apiapple.Repositories;
using Newtonsoft.Json;
using RestSharp;

namespace apiapple.Service
{
    public class ApiService : IApiService
    {
        private const string BaseUrl = "https://rss.itunes.apple.com/api/v1/us/podcasts/top-podcasts/all/100/explicit.json";
        private readonly IFeedRepository _feedRepository;
        private readonly IGenereRepository _genereRepository;
        private readonly IPodcastRepository _podcastRepository;
        private readonly IPodcastGenereRepository _podcastGenereRepository;
        private readonly IUnitOfWork _unitOfWork;
        public ApiService(IFeedRepository feedRepository, IUnitOfWork unitOfWork, IGenereRepository genereRepository, IPodcastGenereRepository songGenereRepository, IPodcastRepository podcastRepository)
        {
            _feedRepository = feedRepository;
            _unitOfWork = unitOfWork;
            _genereRepository = genereRepository;
            _podcastGenereRepository = songGenereRepository;
            _podcastRepository = podcastRepository;
        }
        public async Task GetAppleApiData()
        {
            try
            {
                var restClient = new RestClient();
                var request = new RestRequest(BaseUrl, Method.GET);

                IRestResponse response = restClient.Execute(request);

                if (!response.IsSuccessful)
                {
                    throw new Exception("Cannot connect With Apple API");
                }

                var feedRoot = JsonConvert.DeserializeObject<RootDto>(response.Content);
                var genres = feedRoot.Feed.Results.SelectMany(x => x.Genres).Select(y => y).GroupBy(z => z.GenereId).Select(z => z.First()).ToList();
                _genereRepository.CreateRange(genres);
                await _unitOfWork.Commit();
                var author = feedRoot.Feed.Author;
                var link = new Link()
                {
                    Alternate = feedRoot.Feed.Links[1].Alternate,
                    Self = feedRoot.Feed.Links[0].Self
                };

                var feed = new Feed
                {
                    Title = feedRoot.Feed.Title,
                    Id = feedRoot.Feed.Id,
                    Copyright = feedRoot.Feed.Copyright,
                    Country = feedRoot.Feed.Country,
                    Icon = feedRoot.Feed.Icon,
                    Updated = feedRoot.Feed.Updated,
                    Author = author,
                    Link = link,
                };

                _feedRepository.Create(feed);
                await _unitOfWork.Commit();


                for (int i = 0; i < feedRoot.Feed.Results.Count; i++)
                {
                    var song = new Podcast
                    {
                        ArtistName = feedRoot.Feed.Results[i].ArtistName,
                        Id = feedRoot.Feed.Results[i].Id,
                        ReleaseDate = feedRoot.Feed.Results[i].ReleaseDate,
                        Name = feedRoot.Feed.Results[i].Name,
                        Kind = feedRoot.Feed.Results[i].Kind,
                        Copyright = feedRoot.Feed.Results[i].Copyright,
                        ArtworkUrl100 = feedRoot.Feed.Results[i].ArtworkUrl100,
                        Url = feedRoot.Feed.Results[i].Url,
                        ArtistId = feedRoot.Feed.Results[i].ArtistId,
                        ContentAdvisoryRating = feedRoot.Feed.Results[i].ContentAdvisoryRating,
                        ArtistUrl = feedRoot.Feed.Results[i].ArtistUrl,
                        FeedId = feedRoot.Feed.Id,
                        Top = i + 1
                    };
                    _podcastRepository.Create(song);
                    await _unitOfWork.Commit();
                }


                for (int i = 0; i < feedRoot.Feed.Results.Count; i++)
                {
                    for (int j = 0; j < feedRoot.Feed.Results[i].Genres.Count; j++)
                    {
                        var songGenere = new PodcastGenere
                        {
                            GenreId = feedRoot.Feed.Results[i].Genres[j].GenereId,
                            PodCastId = feedRoot.Feed.Results[i].Id
                        };
                        _podcastGenereRepository.Create(songGenere);
                        await _unitOfWork.Commit();
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Cannot connect With Apple API");

            }

        }
    }
}
