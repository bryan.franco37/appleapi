﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.Models;
using Microsoft.EntityFrameworkCore;

namespace apiapple.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<Feed> Feeds { get; set; }
        public DbSet<Podcast> PodCasts { get; set; }
        public DbSet<PodcastGenere> PodcastGeneres { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Genere> Generes { get; set; }
        public DbSet<Link> Links { get; set; }

    }
}
