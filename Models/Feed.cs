﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace apiapple.Models
{
    public class Feed
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "authorId")]
        public int AuthorId { get; set; }
        public virtual Author Author { get; set; }
        public int LinkId { get; set; }
        public virtual Link Link { get; set; }
        [JsonProperty(PropertyName = "copyright")]
        public string Copyright { get; set; }
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }
        [JsonProperty(PropertyName = "icon")]
        public string Icon { get; set; }
        [JsonProperty(PropertyName = "updated")]
        public DateTime Updated { get; set; }
        [JsonProperty(PropertyName = "results")]
        public virtual List<Podcast> Results { get; set; }
    }
}
