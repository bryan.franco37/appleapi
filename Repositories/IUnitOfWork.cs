﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiapple.Repositories
{
    public interface IUnitOfWork
    {
        Task Commit();

    }
}
