﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace apiapple.DTO
{
    public class LinkDto
    {
        public int id { get; set; }
        [JsonProperty(PropertyName = "alternate")]
        public string Alternate { get; set; }
        [JsonProperty(PropertyName = "self")]
        public string Self { get; set; }
    }
}
