﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace apiapple.Migrations
{
    public partial class initialmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Authors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    Uri = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Generes",
                columns: table => new
                {
                    GenereId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Generes", x => x.GenereId);
                });

            migrationBuilder.CreateTable(
                name: "Links",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Self = table.Column<string>(nullable: true),
                    Alternate = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Links", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Feeds",
                columns: table => new
                {
                    Title = table.Column<string>(nullable: true),
                    Id = table.Column<string>(nullable: false),
                    AuthorId = table.Column<int>(nullable: false),
                    LinkId = table.Column<int>(nullable: false),
                    Copyright = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Icon = table.Column<string>(nullable: true),
                    Updated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feeds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Feeds_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Feeds_Links_LinkId",
                        column: x => x.LinkId,
                        principalTable: "Links",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PodCasts",
                columns: table => new
                {
                    ArtistName = table.Column<string>(nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ReleaseDate = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Kind = table.Column<string>(nullable: true),
                    Copyright = table.Column<string>(nullable: true),
                    ArtworkUrl100 = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true),
                    ArtistId = table.Column<string>(nullable: true),
                    ArtistUrl = table.Column<string>(nullable: true),
                    ContentAdvisoryRating = table.Column<string>(nullable: true),
                    FeedId = table.Column<string>(nullable: true),
                    Top = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PodCasts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PodCasts_Feeds_FeedId",
                        column: x => x.FeedId,
                        principalTable: "Feeds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PodcastGeneres",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PodCastId = table.Column<int>(nullable: false),
                    GenreId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PodcastGeneres", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PodcastGeneres_Generes_GenreId",
                        column: x => x.GenreId,
                        principalTable: "Generes",
                        principalColumn: "GenereId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PodcastGeneres_PodCasts_PodCastId",
                        column: x => x.PodCastId,
                        principalTable: "PodCasts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Feeds_AuthorId",
                table: "Feeds",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Feeds_LinkId",
                table: "Feeds",
                column: "LinkId");

            migrationBuilder.CreateIndex(
                name: "IX_PodcastGeneres_GenreId",
                table: "PodcastGeneres",
                column: "GenreId");

            migrationBuilder.CreateIndex(
                name: "IX_PodcastGeneres_PodCastId",
                table: "PodcastGeneres",
                column: "PodCastId");

            migrationBuilder.CreateIndex(
                name: "IX_PodCasts_FeedId",
                table: "PodCasts",
                column: "FeedId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PodcastGeneres");

            migrationBuilder.DropTable(
                name: "Generes");

            migrationBuilder.DropTable(
                name: "PodCasts");

            migrationBuilder.DropTable(
                name: "Feeds");

            migrationBuilder.DropTable(
                name: "Authors");

            migrationBuilder.DropTable(
                name: "Links");
        }
    }
}
