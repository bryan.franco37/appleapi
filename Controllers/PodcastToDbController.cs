﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.Repositories;
using apiapple.Service;
using Microsoft.AspNetCore.Mvc;

namespace apiapple.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PodcastToDbController : ControllerBase
    {
        private readonly IApiService _apiService;


        public PodcastToDbController(IApiService apiService)
        {
            _apiService = apiService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            await _apiService.GetAppleApiData();
            return Ok("Database Filled Successfully!!");
        }
    }
}
