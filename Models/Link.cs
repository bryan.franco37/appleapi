﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace apiapple.Models
{
    public class Link
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "self")]
        public string Self { get; set; }
        [JsonProperty(PropertyName = "alternate")]
        public string Alternate { get; set; }
    }
}
