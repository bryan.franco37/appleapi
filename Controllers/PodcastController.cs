﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.DTO;
using apiapple.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace apiapple.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PodcastController : ControllerBase
    {
        private readonly IFeedRepository _feedRepository;
        private readonly IPodcastRepository _podCastRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PodcastController(IFeedRepository feedRepository, IPodcastRepository podCastRepository, IUnitOfWork unitOfWork)
        {
            _feedRepository = feedRepository;
            _podCastRepository = podCastRepository;
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<ActionResult<FeedDto>> Get()
        {
            var feed = await _feedRepository.GetFeed();

            if (feed.Id == null)
            {
                return NotFound("Database Empty, to fill database go to: /api/PodCastToDb");
            }
            return await _feedRepository.GetFeed();
        }

        [HttpGet("[action]")]
        public async Task<List<PodcastDto>> GetTop20()
        {
            return await _podCastRepository.GetTop20();
        }


        [HttpGet("[action]")]
        public async Task<List<PodcastDto>> GetLast20()
        {
            return await _podCastRepository.GetLast20();
        }

        [HttpGet("{name}")]
        public async Task<ActionResult<PodcastDto>> FindPodCastByName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return BadRequest();
            }
            var podcast = await _podCastRepository.GetPodCastByName(name);

            if (podcast.Id == 0)
            {
                return NotFound("Podcast Not Found ");
            }

            return podcast;
        }



        [HttpDelete("{id}")]
        public async Task<ActionResult<PodcastDto>> Remove(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            var podCast = await _podCastRepository.GetPodCastById(id);
            if (podCast == null)
            {
                return NotFound("Podcast Not Found");
            }
            _podCastRepository.Remove(podCast);
            await _unitOfWork.Commit();
            return Ok(podCast);
        }
    }
}
