﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.Models;
using Newtonsoft.Json;

namespace apiapple.DTO
{
    public class PodcastDto
    {
        [JsonProperty(PropertyName = "artistName")]
        public string ArtistName { get; set; }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "releaseDate")]
        public string ReleaseDate { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "kind")]
        public string Kind { get; set; }

        [JsonProperty(PropertyName = "copyright")]
        public string Copyright { get; set; }

        [JsonProperty(PropertyName = "artworkUrl100")]
        public string ArtworkUrl100 { get; set; }

        [JsonProperty(PropertyName = "genres")]
        public virtual List<Genere> Genres { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "artistId")]
        public string ArtistId { get; set; }

        [JsonProperty(PropertyName = "artistUrl")]
        public string ArtistUrl { get; set; }

        [JsonProperty(PropertyName = "contentAdvisoryRating")]
        public string ContentAdvisoryRating { get; set; }
        public int Top { get; set; }
    }
}
