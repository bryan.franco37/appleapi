﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.DTO;
using apiapple.Models;

namespace apiapple.Repositories
{
    public interface IPodcastRepository
    {
        Task<PodcastDto> GetPodCastByName(string name);
        Task<Podcast> GetPodCastById(int id);
        Task<List<PodcastDto>> GetTop20();
        Task<List<PodcastDto>> GetLast20();
        Task CreateRange(List<Podcast> postCasts);
        void Create(Podcast podCast);
        void Remove(Podcast podCast);
    }
}
