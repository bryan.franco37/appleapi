﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.Models;

namespace apiapple.Repositories
{
    public interface IPodcastGenereRepository
    {
        void Create(PodcastGenere podcastGenere);
    }
}
