﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.Data;
using apiapple.Models;

namespace apiapple.Repositories
{
    public class GenereRepository :IGenereRepository
    {
        private readonly DataContext _dataContext;

        public GenereRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void CreateRange(List<Genere> genres)
        {
            _dataContext.Generes.AddRange(genres);
        }
    }
}
