﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.Data;
using apiapple.DTO;
using apiapple.Models;
using Microsoft.EntityFrameworkCore;

namespace apiapple.Repositories
{
    public class PodcastRepository : IPodcastRepository
    {
        private readonly DataContext _dataContext;

        public PodcastRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<PodcastDto> GetPodCastByName(string name)
        {
            var podcast = await _dataContext.PodCasts.Include(x => x.PodcastGeneres).ThenInclude(x => x.Genre)
                .FirstOrDefaultAsync(x => x.Name == name);

            if (podcast == null)
            {
                return new PodcastDto();
            }

            var podCastDto = new PodcastDto
            {
                ArtistName = podcast.ArtistName,
                Id = podcast.Id,
                ReleaseDate = podcast.ReleaseDate,
                Name = podcast.Name,
                Kind = podcast.Kind,
                Copyright = podcast.Copyright,
                ArtworkUrl100 = podcast.ArtworkUrl100,
                Url = podcast.Url,
                ArtistId = podcast.ArtistId,
                ContentAdvisoryRating = podcast.ContentAdvisoryRating,
            };
            podCastDto.Genres = podcast.PodcastGeneres.Select(y => y.Genre).ToList();
            return podCastDto;
        }

        public async Task<Podcast> GetPodCastById(int id)
        {
            var podcast = await _dataContext.PodCasts.Include(x => x.PodcastGeneres).ThenInclude(x => x.Genre)
                .FirstOrDefaultAsync(x => x.Id == id);


            return podcast;
        }

        public async Task<List<PodcastDto>> GetTop20()
        {
            var podcast = await _dataContext.PodCasts.OrderBy(x => x.Top).Where(x => x.Top >= 1 && x.Top <= 20).Include(x => x.PodcastGeneres).ThenInclude(x => x.Genre).ToListAsync();

            if (podcast == null)
            {
                return new List<PodcastDto>();
            }

            var podCastDto = podcast.OrderBy(x => x.Top).Select(x => new PodcastDto
            {
                ArtistName = x.ArtistName,
                Id = x.Id,
                ReleaseDate = x.ReleaseDate,
                Name = x.Name,
                Kind = x.Kind,
                Copyright = x.Copyright,
                ArtworkUrl100 = x.ArtworkUrl100,
                Url = x.Url,
                ArtistId = x.ArtistId,
                ContentAdvisoryRating = x.ContentAdvisoryRating,
                Top = x.Top,
                Genres = x.PodcastGeneres.Select(y => y.Genre).ToList()
            }).ToList();


            return podCastDto;

        }



        public async Task<List<PodcastDto>> GetLast20()
        {
            var podcast = await _dataContext.PodCasts.OrderBy(x => x.Top).Where(x => x.Top >= 80 && x.Top <= 100).Include(x => x.PodcastGeneres).ThenInclude(x => x.Genre).ToListAsync();

            if (podcast == null)
            {
                return new List<PodcastDto>();
            }

            var podCastDto = podcast.OrderBy(x => x.Top).Select(x => new PodcastDto
            {
                ArtistName = x.ArtistName,
                Id = x.Id,
                ReleaseDate = x.ReleaseDate,
                Name = x.Name,
                Kind = x.Kind,
                Copyright = x.Copyright,
                ArtworkUrl100 = x.ArtworkUrl100,
                Url = x.Url,
                ArtistId = x.ArtistId,
                Top = x.Top,
                ContentAdvisoryRating = x.ContentAdvisoryRating,
                Genres = x.PodcastGeneres.Select(y => y.Genre).ToList()
            }).ToList();


            return podCastDto;
        }


        public async Task CreateRange(List<Podcast> results)
        {
            await _dataContext.PodCasts.AddRangeAsync(results);
        }

        public void Create(Podcast result)
        {
            _dataContext.PodCasts.Add(result);
        }

        public void Remove(Podcast podCast)
        {
            _dataContext.Remove(podCast);
        }
    }
}
