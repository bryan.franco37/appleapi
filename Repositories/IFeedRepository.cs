﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.DTO;
using apiapple.Models;

namespace apiapple.Repositories
{
    public interface IFeedRepository
    {
        Task<FeedDto> GetFeed();
        void Create(Feed feed);
    }
}
