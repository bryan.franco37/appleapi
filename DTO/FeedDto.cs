﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiapple.Models;
using Newtonsoft.Json;

namespace apiapple.DTO
{
    public class FeedDto
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "author")]
        public virtual Author Author { get; set; }
        public virtual Link Link { get; set; }
        public virtual List<LinkDto> Links { get; set; }
        [JsonProperty(PropertyName = "copyright")]
        public string Copyright { get; set; }
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }
        [JsonProperty(PropertyName = "genres")]
        public virtual List<Genere> Genres { get; set; }
        [JsonProperty(PropertyName = "icon")]
        public string Icon { get; set; }
        [JsonProperty(PropertyName = "updated")]
        public DateTime Updated { get; set; }
        [JsonProperty(PropertyName = "results")]
        public virtual List<PodcastDto> Results { get; set; }
    }
}
