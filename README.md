This is a project to give solution to challange number 1:



---------How to run Project -----------------
To run this project you have to:

1- Clone this repository 

2- Download netcore 2.1, you can download it from here: https://dotnet.microsoft.com/download/dotnet-core/2.1

3- After install netcore 2.1, go into folder project open a console (linux bash, windows powershell) and execute,  dotnet restore

4- After project compile you are ready to run it, execute this command, dotnet run

5 -Once the project are running you will in the console the access url, https://localhost:5001

6- First you hace get into this url, https://localhost:5001/api/PodCastToDb, this url is to fill database directly from the api provide in test description.

7- After you visit the before url you will see a success message.

8- Now the database is filled, you can go to this url, https://localhost:5001/api/PodCast, and you will see all the information from the database.

------------ Challange Solution  -------------


1- A service to provide a search lookup within the podcasts (at least by name, but is open to any suggestions), 
   
   to search by name only have to go to this url : https://localhost:5001/api/PodCast/Unlocking Us with Brené Brown, where the information after the last slash is going to be 
   the search parameter in this the name.
   
2-  A service that would allow to save the top 20 podcasts to a separate JSON File, for that you have to go to this url, https://localhost:5001/api/PodCast/Gettop20,
    it will return the top 20,
    
3- A service to replace the top 20 podcasts for the bottom 20 to said JSON File, go to this url, https://localhost:5001/api/PodCast/GetLast20,
   you will see only the last top 20

4- A service to remove a podcast, using a given identifier, to delete a podcast go to this url, https://localhost:5001/api/PodCast/1268527882,
   you have to send a delete request to the url and the information at the end of the url is going to be the id of the podcast.
   
5- Use a Database (any of your preference), instead of the JSON File. Include a create schema in the repo and instructions on how to implement it. 
   This solution has added a sqlite database you dont't have to do nothing to implement, the database is already implemented. You can see it in root folder of the project,
   the file is named podcastApp.db.