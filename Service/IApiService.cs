﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiapple.Service
{
    public interface IApiService
    {
        Task GetAppleApiData();

    }
}
